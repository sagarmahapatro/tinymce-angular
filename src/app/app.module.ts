import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import tinymce from 'tinymce/tinymce';
import { TinymceComponent } from './tinymce/tinymce.component';
import { EditorModule } from '../../tinymce-angular-component/src/editor/editor.module';


@NgModule({
  declarations: [
    AppComponent,
    TinymceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    EditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
