import { Component, OnInit } from '@angular/core';

import tinymce from 'tinymce/tinymce';
import * as $ from 'jquery'

@Component({
  selector: 'app-tinymce',
  templateUrl: './tinymce.component.html',
  styleUrls: ['./tinymce.component.css']
})
export class TinymceComponent implements OnInit {

  tinyMceSettings ={
    theme_url: 'assets/themes/silver/theme.js',
    skin_url: 'assets/tinymce/ui/oxide',
    height: 320,
    inline: false,
    toolbar1: 'demoButton bold italic',
    menubar: false
  } 

  constructor() { }

  ngOnInit() {
/*
  // Initialize the app
  tinymce.init({
    theme_url: 'assets/themes/silver/theme.js',
    skin_url: 'assets/tinymce/ui/oxide',
    selector: '#tinymce',
    height: 320,
    toolbar1: 'demoButton bold italic',
    menubar: false
  });

  console.log(tinymce.init);*/
  }

}
